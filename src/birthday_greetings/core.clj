(ns birthday-greetings.core
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [postal.core :refer [send-message]]
            [java-time :as jt])
  (:gen-class))

(defn short-date
  "Extract the month and day from a local-date."
  [localdate]
  (jt/as localdate :month-of-year :day-of-month))

(defn compare-dates
  "Return true if two local-date values are the same day of the year."
  [d-o-b date]
  (= (short-date date) (short-date d-o-b)))

(defn parse-date
  "Parse a string into a local-date value."
  [date]
  (jt/local-date "yyyy/MM/dd" date))

(defn parse-row
  "Parse a row from the employees file into a map."
  [row]
  (let [[last-name first-name d-o-b email] (map string/trim row)]
    {:first-name first-name, :last-name last-name, :d-o-b (parse-date d-o-b), :email email}))

(defn is-birthday
  "Returns true if the date is the provided user's birthday."
    [user date]
    (compare-dates (:d-o-b user) date))

(def email-template "Happy Birthday, dear %NAME%")

(defn email-body
  [user]
  (clojure.string/replace email-template #"%NAME%" (:first-name user)))

(def from-address "birthdays@example.com")

(defn send-email
  "Generate a templated email and send it via SMTP."
  [smtp-host smtp-port user]
  (let [conn {:host smtp-host, :port smtp-port}
        mail {:from from-address,
              :to [(:email user)],
              :body (email-body user),
              :subject "Happy Birthday!"}]
    (send-message conn mail)))

(defn -main
  "Send the birthday greetings."
  [& args]
  (let [today (parse-date "2019/03/11")]
    (with-open [reader (io/reader "employee_data.txt")]
      (doall
        (->> (csv/read-csv reader)
             (drop 1) ; drop the header
             (map parse-row)
             (filter #(is-birthday % today))
             (map #(send-email "localhost" 2525 %)))))))

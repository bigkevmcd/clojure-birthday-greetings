#!/usr/bin/env python
import smtpd
import asyncore

if __name__ == "__main__":
    smtp_server = smtpd.DebuggingServer(('localhost', 2525), None)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        smtp_server.close()

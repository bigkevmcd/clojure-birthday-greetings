(ns birthday-greetings.core-test
  (:require [clojure.test :refer :all]
            [java-time :as jt]
            [birthday-greetings.core :refer :all]))

(deftest parse-date-test
  (testing "parsing the d-o-b string"
    (is (= (jt/local-date 1982 10 8) (parse-date "1982/10/08")))))

(deftest compare-date-test
  (testing "comparing short dates"
    (is (= true (compare-dates (jt/local-date 1982 10 8) (jt/local-date 2019 10 8))))
    (is (= false (compare-dates (jt/local-date 1982 10 8) (jt/local-date 2019 9 8))))))
